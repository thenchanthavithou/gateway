package com.micrpservice.apigateway.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatewayController {
    @GetMapping("/fallback/error-message")
    public ResponseEntity<String> handleGateway() {
        return new ResponseEntity<String>("Sorry bro", HttpStatus.BAD_GATEWAY);
    }
}
